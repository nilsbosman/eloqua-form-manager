<?php
/*
Plugin Name: Eloqua Form Manager
Description: Easily manage all your eloqua forms. Adds AJAX, a Honeypot field, Google Recaptcha and more.
Version:     1.09
Author:      Nils Bosman
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
*/

// Update Plugin
require 'efm_auto-updater/plugin-update-checker.php';

$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/nilsbosman/eloqua-form-manager/',
	__FILE__,
	'eloqua-form-manager'
);
// Branch to update from
$myUpdateChecker->setBranch('master');


// Define global vars for plugin
define( 'PLUGIN_PATH', plugin_dir_path(__FILE__) );
define( 'PLUGIN_URL', plugin_dir_url(__FILE__) );

// Create database table and optionaly save some sample data
include_once( PLUGIN_PATH . 'efm_parts/efm_database.php' );

// Create WP_List page
include_once( PLUGIN_PATH . 'efm_parts/efm_custom-table-list.php' );

// Registers admin pages
include_once( PLUGIN_PATH . 'efm_parts/efm_admin-pages.php' );

// Creates Add/Edit page
include_once( PLUGIN_PATH . 'efm_parts/efm_add-edit-form.php' );

// Shortcode logic for including forms in pages/posts
include_once( PLUGIN_PATH . 'efm_parts/efm_form-shortcode.php' );

