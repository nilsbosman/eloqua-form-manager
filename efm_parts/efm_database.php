<?php

/**
 * PART 1. Defining Custom Database Table
 * ============================================================================
 *
 * In this part you are going to define custom database table,
 * create it, update, and fill with some dummy data
 *
 * http://codex.wordpress.org/Creating_Tables_with_Plugins
 *
 * In case your are developing and want to check plugin use:
 *
 * DROP TABLE IF EXISTS wp_cte;
 * DELETE FROM wp_options WHERE option_name = 'efm_install_data';
 *
 * to drop table and option
 */

/**
 * $efm_db_version - holds current database version
 * and used on plugin update to sync database tables
 */
global $efm_db_version;
$efm_db_version = '1.1'; // version changed from 1.0 to 1.1

/**
 * register_activation_hook implementation
 *
 * will be called when user activates plugin first time
 * must create needed database tables
 */
function efm_install()
{
    global $wpdb;
    global $efm_db_version;

    $table_name = $wpdb->prefix . 'eloqua_form_manager'; // do not forget about tables prefix

    // sql to create your table
    // NOTICE that:
    // 1. each field MUST be in separate line
    // 2. There must be two spaces between PRIMARY KEY and its name
    //    Like this: PRIMARY KEY[space][space](id)
    // otherwise dbDelta will not work
    $sql = "CREATE TABLE " . $table_name . " (
      id int(11) NOT NULL AUTO_INCREMENT,
        form_name tinytext NOT NULL,
        form_upload_name MEDIUMTEXT NOT NULL,
        PRIMARY KEY  (id)
    );";

    // we do not execute sql directly
    // we are calling dbDelta which cant migrate database
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    dbDelta($sql);

    // save current database version for later use (on upgrade)
    add_option('efm_db_version', $efm_db_version);

    /**
     * [OPTIONAL] Example of updating to 1.1 version
     *
     * If you develop new version of plugin
     * just increment $efm_db_version variable
     * and add following block of code
     *
     * must be repeated for each new version
     * in version 1.1 we change email field
     * to contain 200 chars rather 100 in version 1.0
     * and again we are not executing sql
     * we are using dbDelta to migrate table changes
     */
    $installed_ver = get_option('efm_db_version');
    if ($installed_ver != $efm_db_version) {
        $sql = "CREATE TABLE " . $table_name . " (
          id int(11) NOT NULL AUTO_INCREMENT,
          form_name tinytext NOT NULL,
          form_html MEDIUMTEXT NOT NULL,
          form_processed_html MEDIUMTEXT NOT NULL,
          PRIMARY KEY  (id)
        );";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);

        // notice that we are updating option, rather than adding it
        update_option('efm_db_version', $efm_db_version);
    }

    // Create uploads dir
    if (!file_exists( PLUGIN_PATH . 'efm_uploads/')) {
        mkdir( PLUGIN_PATH . 'efm_uploads/', 0777, true);
    }
}

register_activation_hook(__FILE__, 'efm_install');


/**
 * Trick to update plugin database, see docs
 */
function efm_update_db_check()
{
    global $efm_db_version;
    if (get_site_option('efm_db_version') != $efm_db_version) {
        efm_install();
    }
}

add_action('plugins_loaded', 'efm_update_db_check');
