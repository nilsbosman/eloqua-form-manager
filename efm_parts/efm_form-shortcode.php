<?php

// hint: registers all our custom shortcodes on init
add_action('init', 'efm_register_shortcodes');

function efm_register_shortcodes()
{
    add_shortcode('eloqua-form', 'efm_form_shortcode');
}

function efm_form_shortcode($args)
{

    // Check if args[id] isn't set or empty;
    if (!isset($args['id']) || ($args['id'] == '')) {
        return '<span style="display: block; color: red; border: 1px solid red; padding: 10px; margin: 20px 0;"><strong>Error:</strong> Invalid form id</span>';
    } else {
        $form_id = (int)$args['id'];

        // TODO: Get array with form info by form_id
        $form_data = get_form_by_id($form_id);
        $form = get_form_html($form_data['form_upload_name']);

        $output = '
        
            <div class="eloqua-form-manager efm__wrapper">
                
                '. $form .'

            </div>
        
        ';
        
        // return our results/html
        return $output;
    }
}


function get_form_by_id($form_id)
{
    global $wpdb;
    $tablename = $wpdb->prefix . 'eloqua_form_manager';
    $form_data = array();
    $form_data = $wpdb->get_row("SELECT * FROM $tablename WHERE id = $form_id", ARRAY_A);
    return $form_data;
}

function get_form_html($form_name)
{
    $uploads_path = PLUGIN_PATH . 'efm_uploads/' . $form_name;
    $raw_form = file_get_contents($uploads_path);
    
    $form = regex_form($raw_form);

    return $form;
}

function regex_form($raw_form)
{
    // Setup regex
    $remove_eloqua_values = "/<eloqua[\s\S]*?\/>/";
    $remove_styles = "/<style[\s\S]*?<\/style>/";
    $remove_whitespaces_textarea = '/(<textarea.*>)(.*)(<\/textarea>)/s';
    $remove_submit_button_styles = '/(input.*type="submit".*style=").*(")/';

    $add_row_to_textarea = '/<textarea/';

    // Execute regex    
    $raw_form = preg_replace($remove_eloqua_values, "", $raw_form);
    $raw_form = preg_replace($remove_styles, "", $raw_form);
    $raw_form = preg_replace($remove_whitespaces_textarea, "$1$3", $raw_form);
    $raw_form = preg_replace($remove_submit_button_styles, "$1$2", $raw_form);

    $raw_form = preg_replace($add_row_to_textarea, '<textarea rows="6"', $raw_form);

    // Return editted form
    $clean_form = $raw_form;
    return $clean_form;
}
