<?php
/**
 * PART 3. Admin page
 * ============================================================================
 *
 * In this part you are going to add admin page for custom table
 *
 * http://codex.wordpress.org/Administration_Menus
 */

/**
 * admin_menu hook implementation, will add pages to list persons and to add new one
 */
function efm_admin_menu()
{
    add_menu_page(
        __('EFM', 'eloqua-form-manager'),
        __('EFM', 'eloqua-form-manager'),
        'activate_plugins',
        'efm_dashboard',
        'efm_forms_overview_handler'
    );
    add_submenu_page(
        'efm_dashboard',
        __('Dashboard', 'eloqua-form-manager'),
        __('Dashboard', 'eloqua-form-manager'),
        'activate_plugins',
        'efm_dashboard',
        'efm_forms_overview_handler'
    );
    // add new will be described in next part
    add_submenu_page(
        'efm_dashboard',
        __('Add new form', 'eloqua-form-manager'),
        __('Add new', 'eloqua-form-manager'),
        'activate_plugins',
        'efm_edit-forms',
        'efm_eloqua_form_handler'
    );
    add_submenu_page(
        'efm_dashboard',
        __('Settings', 'eloqua-form-manager'),
        __('Settings', 'eloqua-form-manager'),
        'activate_plugins',
        'efm_settings',
        'efm_settings_handler'
    );

}
add_action('admin_menu', 'efm_admin_menu');


/**
 * List page handler
 *
 * This function renders our custom table
 * Notice how we display message about successfull deletion
 * Actualy this is very easy, and you can add as many features
 * as you want.
 *
 * Look into /wp-admin/includes/class-wp-*-list-table.php for examples
 */
function efm_forms_overview_handler()
{
    global $wpdb;

    $table = new Forms_List_Table();
    $table->prepare_items();

    $message = '';
    if ('delete' === $table->current_action()) {
        $message = '<div class="updated below-h2" id="message"><p>' . sprintf(__('Items deleted: %d', 'eloqua-form-manager'), count($_REQUEST['id'])) . '</p></div>';
    } ?>
    <div class="wrap">

        <div class="icon32 icon32-posts-post" id="icon-edit"><br></div>

        <h2><?php _e('Eloqua Forms', 'eloqua-form-manager')?>
            <a class="add-new-h2" href="<?php echo get_admin_url(get_current_blog_id(), 'admin.php?page=efm_edit-forms'); ?>"><?php _e('Add new', 'eloqua-form-manager')?></a>
        </h2>

        <?php echo $message; ?>

        <form id="persons-table" method="GET">
            <input type="hidden" name="page" value="<?php echo $_REQUEST['page'] ?>"/>
            <?php $table->display() ?>
        </form>
    </div>
<?php
}

function efm_settings_handler()
{
    ?>
    <h2>
        <?php _e('Settings', 'eloqua-form-manager')?>
    </h2>
    <?php
}
