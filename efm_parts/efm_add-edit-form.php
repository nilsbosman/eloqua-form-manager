<?php
/**
 * PART 4. Form for adding andor editing row
 * ============================================================================
 *
 * In this part you are going to add admin page for adding andor editing items
 * You cant put all form into this function, but in this example form will
 * be placed into meta box, and if you want you can split your form into
 * as many meta boxes as you want
 *
 * http://codex.wordpress.org/Data_Validation
 * http://codex.wordpress.org/Function_Reference/selected
 */

/**
 * Form page handler checks is there some data posted and tries to save it
 * Also it renders basic wrapper in which we are callin meta box render
 */
function efm_eloqua_form_handler()
{
    global $wpdb;
    $table_name = $wpdb->prefix . 'eloqua_form_manager'; // do not forget about tables prefix

    $messages = [];
    $notices = [];

    // this is default $item which will be used for new records
    $default = array(
        'id' => 0,
        'form_name' => '',
        'form_upload_name' => '',
    );

    //echo '<pre>'; print_r($_REQUEST); echo '</pre>';

    // here we are verifying does this request is post back and have correct nonce
    if (isset($_REQUEST['nonce']) && wp_verify_nonce($_REQUEST['nonce'], basename(__FILE__))) {

        // combine our default item with request params
        $item = shortcode_atts( $default, $_REQUEST );
        
        // validate data, and if all ok save item to database
        // if id is zero insert otherwise update
        $item_valid = efm_validate_person($item);
        if ($item_valid === true) {

            switch ($_FILES['form_upload_html']['error']) {
                case UPLOAD_ERR_INI_SIZE:
                    $errors[] = "The uploaded file exceeds the upload_max_filesize directive in php.ini";
                    break;
                case UPLOAD_ERR_FORM_SIZE:
                    $errors[] = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form";
                    break;
                case UPLOAD_ERR_PARTIAL:
                    $errors[] = "The uploaded file was only partially uploaded";
                    break;
                case UPLOAD_ERR_NO_FILE:
                    $errors[] = "No file was uploaded";
                    break;
                case UPLOAD_ERR_NO_TMP_DIR:
                    $errors[] = "Missing a temporary folder";
                    break;
                case UPLOAD_ERR_CANT_WRITE:
                    $errors[] = "Failed to write file to disk";
                    break;
                case UPLOAD_ERR_EXTENSION:
                    $errors[] = "File upload stopped by extension";
                    break;

                default:
                    $errors[] = "Unknown upload error";
                    break;
            }

            if ($_FILES['form_upload_html']['error'] === UPLOAD_ERR_OK) {


                //echo '<pre>'; print_r($_FILES); echo '</pre>';

                $messages[] = "File upload success";

                //returns feedback from the uploading process
                $upload_feedback = efm_upload_file();

                // Assign errors from upload to main error arrays
                foreach ($upload_feedback['errors'] as $upload_errors) {
                    $errors[] = $upload_errors;
                }
                foreach ($upload_feedback['messages'] as $upload_messages) {
                    $messages[] = $upload_messages;
                }

                // get the contents of the file and paste duplicate it inside the form_html field
                $eloqua_form_html = file_get_contents(PLUGIN_PATH . 'efm_uploads/' . $_FILES['form_upload_html']['name']);

                // prepare file name for database
                $item['form_upload_name'] = $_FILES['form_upload_html']['name'];

            }

            if ($item['id'] == 0) {
                $result = $wpdb->insert($table_name, $item);
                $item['id'] = $wpdb->insert_id;
                if ($result !== false) {
                    $messages[] = __('Item was successfully saved', 'eloqua-form-manager');
                } else {
                    $notices[] = __('There was an error while saving item', 'eloqua-form-manager');
                }
            } else {
                
                $result = $wpdb->update($table_name, $item, array('id' => $item['id']));
                //echo $wpdb->print_error();

                if ($result !== false) {
                    $messages[] = __('Item was successfully updated', 'eloqua-form-manager');
                } else {
                    $notices[] = __('There was an error while updating item', 'eloqua-form-manager');
                }
            }
        } else {
            // if $item_valid not true it contains error message(s)
            $notices[] = $item_valid;
        }
    } else {
        // if this is not post back we load item to edit or give new one to create
        $item = $default;
        if (isset($_REQUEST['id'])) {
            $item = $wpdb->get_row($wpdb->prepare("SELECT * FROM $table_name WHERE id = %d", $_REQUEST['id']), ARRAY_A);
            if (!$item) {
                $item = $default;
                $notices[] = __('Item not found', 'eloqua-form-manager');
            }
        }
    }

    // here we adding our custom meta box
    add_meta_box('eloqua_form_meta_box', 'Person data', 'efm_eloqua_form_meta_box_handler', 'eloqua-form', 'normal', 'default'); ?>
    
    <div class="wrap">
        <div class="icon32 icon32-posts-post" id="icon-edit"><br></div>

        <h2>
            <?php _e('Edit/Add Eloqua Form', 'eloqua-form-manager')?>
            <a class="add-new-h2" href="<?php echo get_admin_url(get_current_blog_id(), 'admin.php?page=efm_dashboard'); ?>"><?php _e('back to list', 'eloqua-form-manager')?></a>
        </h2>

        <?php if (!empty($notices)): ?>
        <div id="notice" class="error">
            <p>
                <?php foreach ($notices as $notice) {
                    echo "$notice <br>";
                } ?>
            </p>
        </div>
        <?php endif; ?>
        <?php if (!empty($messages)): ?>
        <div id="message" class="updated">
            <p>
                <?php foreach ($messages as $message) {
                    echo "$message <br>";
                } ?>
            </p>
        </div>
        <?php endif; ?>

        <form id="form" enctype="multipart/form-data" method="POST">
            <input type="hidden" name="nonce" value="<?php echo wp_create_nonce(basename(__FILE__))?>"/>
            <?php /* NOTICE: here we storing id to determine will be item added or updated */ ?>
            <input type="hidden" name="id" value="<?php echo $item['id'] ?>"/>

            <div class="metabox-holder" id="poststuff">
                <div id="post-body">
                    <div id="post-body-content">
                        <?php /* And here we call our custom meta box */ ?>
                        <?php do_meta_boxes('eloqua-form', 'normal', $item); ?>
                        <input type="submit" value="<?php _e('Save', 'eloqua-form-manager')?>" id="submit" class="button-primary" name="submit">
                    </div>
                </div>
            </div>
        </form>
    </div>
<?php
}

/**
 * This function renders our custom meta box
 * $item is row
 *
 * @param $item
 */
function efm_eloqua_form_meta_box_handler($item)
{
    ?>
<table cellspacing="2" cellpadding="5" style="width: 100%;" class="form-table">
    <tbody>
    <tr class="form-field">
        <th valign="top" scope="row">
            <label for="form_name"><?php _e('Form name', 'eloqua-form-manager')?></label>
        </th>
        <td>
            <input id="form_name" name="form_name" type="text" value="<?php echo esc_attr($item['form_name'])?>" placeholder="<?php _e('Form name', 'eloqua-form-manager')?>" required>
        </td>
    </tr>
    <tr class="form-field">
        <th valign="top" scope="row">
            <label for="form_upload_html"><?php _e('Upload Form HTML', 'eloqua-form-manager')?></label>
        </th>
        <td>
            <input id="form_upload_html" name="form_upload_html" type="file">
        </td>
    </tr>
    <tr class="form-field">
        <th valign="top" scope="row">
            <label for="form_upload_name"><?php _e('Form filename', 'eloqua-form-manager')?></label>
        </th>
        <td>
            <input id="form_upload_name" name="form_upload_name" type="text" value="<?php echo esc_attr($item['form_upload_name'])?>" placeholder="<?php _e('Form filename', 'eloqua-form-manager')?>">
        </td>
    </tr>
    </tbody>
</table>
<?php
}

/**
 * Simple function that validates data and retrieve bool on success
 * and error message(s) on error
 *
 * @param $item
 * @return bool|string
 */
function efm_validate_person($item)
{
    $messages = array();

    // if (empty($item['name'])) {
    //     $messages[] = __('Name is required', 'eloqua-form-manager');
    // }
    // if (!empty($item['email']) && !is_email($item['email'])) {
    //     $messages[] = __('E-Mail is in wrong format', 'eloqua-form-manager');
    // }
    //if(!empty($item['age']) && !absint(intval($item['age'])))  $messages[] = __('Age can not be less than zero');
    //if(!empty($item['age']) && !preg_match('/[0-9]+/', $item['age'])) $messages[] = __('Age must be number');
    //...

    if (empty($messages)) {
        return true;
    }
    return implode('<br />', $messages);
}

function efm_upload_file()
{
    $currentDir = PLUGIN_PATH;
    $uploadDirectory = 'efm_uploads/';

    $fileExtensions = ['txt','html'];

    $fileName = $_FILES['form_upload_html']['name'];
    $fileSize = $_FILES['form_upload_html']['size'];
    $fileTmpName  = $_FILES['form_upload_html']['tmp_name'];
    $fileType = $_FILES['form_upload_html']['type'];
    $tmp = explode('.', $fileName);
    $fileExtension = strtolower(end($tmp));

    $uploadPath = $currentDir . $uploadDirectory . basename($fileName);

    $upload_feedback['errors'] = [];
    $upload_feedback['messages'] = [];


    if (! in_array($fileExtension, $fileExtensions)) {
        $upload_feedback['errors'][] = "Upload: This file extension is not allowed. Please upload a JPEG or PNG file";
    }

    if ($fileSize > 100000) {
        $upload_feedback['errors'][] = "Upload: This file is more than 100kb. Sorry, it has to be less than or equal to 100kb";
    }

    if (empty($errors)) {

        //$didUpload = false;
        $didUpload = move_uploaded_file($fileTmpName, $uploadPath);

        if ($didUpload) {
            //$upload_feedback['messages'][] = "Upload: The file: " . basename($fileName) . " has been uploaded";
        } else {
            $upload_feedback['errors'][] = "Upload: An error occurred somewhere. Try again or contact the admin";
        }
    }

    return $upload_feedback;
}

/**
 * Do not forget about translating your plugin, use __('english string', 'your_uniq_plugin_name') to retrieve translated string
 * and _e('english string', 'your_uniq_plugin_name') to echo it
 * in this example plugin your_uniq_plugin_name == eloqua-form-manager
 *
 * to create translation file, use poedit FileNew catalog...
 * Fill name of project, add "." to path (ENSURE that it was added - must be in list)
 * and on last tab add "__" and "_e"
 *
 * Name your file like this: [my_plugin]-[ru_RU].po
 *
 * http://codex.wordpress.org/Writing_a_Plugin#Internationalizing_Your_Plugin
 * http://codex.wordpress.org/I18n_for_WordPress_Developers
 */
function efm_languages()
{
    load_plugin_textdomain('eloqua-form-manager', false, dirname(plugin_basename(__FILE__)));
}

add_action('init', 'efm_languages');
